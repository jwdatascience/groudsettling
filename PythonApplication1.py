import numpy as np
import pandas as pd
import glob, os
import xlrd
import itertools
import matplotlib.pyplot as plt
from pandas import *
from sklearn import preprocessing
from sklearn import svm
from sklearn.svm import SVC  
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import classification_report, confusion_matrix


d = {}
wb = xlrd.open_workbook('C:/Users/Surface/Downloads/labels.xlsx')
sh = wb.sheet_by_index(0)   
for i in range(168):
    cell_value_class = float(sh.cell(i,1).value)
    cell_value_id = int(sh.cell(i,0).value)
    d[cell_value_id] = cell_value_class
feats=[]
labels=[]

print(d.keys())


data=(pd.read_excel('C:/Users/Surface/Downloads/depth.xlsx',skiprows=3, header=None)
    .dropna(how='all', axis=1))
data = data.drop(data.columns[0], 1)
val = data.values.flatten()
#stdx=data.std(axis=0).values
#featsSorted=np.argsort(stdx)
#keep = featsSorted[-10:]
keep=np.array([7, 16, 17])

os.chdir("C:/Users/Surface/Downloads/WestLineData/data")
filelist=glob.glob("*.xlsx")
numslist = []
for file in filelist:
    filename_parse=file.split(".")
    num=filename_parse[0] 
    os.rename(file, num+'.xlsx')
    numslist.append(int(num))

#filelist=glob.glob("*.xlsx")
nums=list(map(int, numslist))
nums.sort()
for i in range(0,len(nums)):
    #file = filelist[i]
    #filePrev = filelist[i-1]
    #fileNext = filelist[i+1]
    #filename_parse=file.split(".")
    num=nums[i]
    if not(num in d.keys()):
        continue
    
    keyList=list(d.keys())
    index=keyList.index(num)
    #filename_parse_prev=filePrev.split(".")
    if (index-1) < 0 or (index+1) > len(keyList)-2:
        continue
    prevNum=keyList[index-1]
    #filename_parse_next=fileNext.split(".")
    nextNum=keyList[index+1]
    feat1=[]
    feat2=[]
    feat=[]
    if num in d.keys():
        depth=val[num]
        
        #read current file
        if not os.path.isfile(str(num)+'.xlsx'):
            continue
        df = (pd.read_excel(str(num)+'.xlsx', skiprows=2)
            .dropna(how='all', axis=1))
        df = df.drop(df.columns[0], 1)
        df = df[df.columns[keep]]
        #print(len(df.columns))
        x = df.values #returns a numpy array
        min_max_scaler = preprocessing.MinMaxScaler()
        x_scaled = min_max_scaler.fit_transform(x)
        df = pd.DataFrame(x_scaled)
        stdx=df.std(axis=0).values
        mean=df.mean(axis=0).values
        feat=np.concatenate((mean,stdx)) 
        feat=np.append(feat, depth)
    #if prevNum in d.keys():
        depthPrev=val[prevNum]
        #read prev file
        noprev= False
        while not os.path.isfile(str(prevNum)+'.xlsx'):
            prevNum-=1
            if prevNum < nums[0]:
                noprev = True
                break
        if noprev:
            continue
        df = (pd.read_excel(str(prevNum)+'.xlsx', skiprows=2)
            .dropna(how='all', axis=1))
        df = df.drop(df.columns[0], 1)
        df = df[df.columns[keep]]
        #print(len(df.columns))
        x = df.values #returns a numpy array
        min_max_scaler = preprocessing.MinMaxScaler()
        x_scaled = min_max_scaler.fit_transform(x)
        df = pd.DataFrame(x_scaled)
        stdx=df.std(axis=0).values
        mean=df.mean(axis=0).values
        feat1=np.concatenate((mean,stdx)) 
        feat1=np.append(feat1, depthPrev)
    #if nextNum in d.keys():
        depthNext=val[nextNum]
        #read next file
        nonext=False
        while not os.path.isfile(str(nextNum)+'.xlsx'):
            nextNum+=1
            if nextNum > nums[-1]:
                nonext = True
                break
        if nonext:
            continue
        df = (pd.read_excel(str(nextNum)+'.xlsx', skiprows=2)
            .dropna(how='all', axis=1))
        df = df.drop(df.columns[0], 1)
        df = df[df.columns[keep]]
        #print(len(df.columns))
        x = df.values #returns a numpy array
        min_max_scaler = preprocessing.MinMaxScaler()
        x_scaled = min_max_scaler.fit_transform(x)
        df = pd.DataFrame(x_scaled)
        stdx=df.std(axis=0).values
        mean=df.mean(axis=0).values
        feat2=np.concatenate((mean,stdx)) 
        feat2=np.append(feat2, depthNext)
    
    #if len(feat) >0 and len(feat1)>0 and len(feat2)>0:
        Feat=list(itertools.chain(feat1,feat,feat2))
        feats.append(Feat)
        if d[num] <= -20:
            labels.append(1)
        elif d[num] > 20:
            labels.append(1)
        else:
            labels.append(0)

print(len(feats))
print(len(labels))

clf = SVC(kernel='rbf',gamma='auto')
#clf = RandomForestClassifier(n_estimators=100)

X_train, X_test, y_train, y_test = train_test_split(feats, labels, test_size=0.1)
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
#print(f1_score(y_test, y_pred, average="macro"))
#print(precision_score(y_test, y_pred, average="macro"))
#print(recall_score(y_test, y_pred, average="macro"))
print(classification_report(y_test, y_pred))

fpr, tpr, _ = roc_curve(y_test, y_pred)
roc_auc = auc(fpr, tpr)

plt.figure()
lw = 2
plt.plot(fpr, tpr, color='darkorange',
         lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic example')
plt.legend(loc="lower right")
plt.show()





